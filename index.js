// Class creation

class Pokemon{
	constructor(name, lvl, hp){
		this.name = name;
		this.level = lvl;
		this.health = hp * 2;
		this.attack = lvl * 2;
	
    // Method Creation
    this.tackle = function(target){
      target.health -= this.attack
      console.log(target.name + "'s health was reduced to " + target.health)

      // Condition of when to faint

      if (target.health < 0){
        console.log(target.name + ' fainted.')
      }
    }

  }
}

// Creating instances
const myPokemon = new Pokemon('Bulbasaur', 3, 30);
const vsPokemon = new Pokemon('Squirtle', 2, 20);


// Invocation

myPokemon.tackle(vsPokemon)
vsPokemon.tackle(myPokemon)
myPokemon.tackle(vsPokemon)
vsPokemon.tackle(myPokemon)
myPokemon.tackle(vsPokemon)
vsPokemon.tackle(myPokemon)
myPokemon.tackle(vsPokemon)
vsPokemon.tackle(myPokemon)
myPokemon.tackle(vsPokemon)
vsPokemon.tackle(myPokemon)
myPokemon.tackle(vsPokemon)
vsPokemon.tackle(myPokemon)
myPokemon.tackle(vsPokemon)



/*
  Reserved code in case needed
    this.faint = function(){
      console.log(this.name + ' fainted ')
    }

*/